//1. select the element
//2. Add Listener to it
//3. write a function which performs job when event occurs
//  document.querySelector("selector").addEventListener("click", function (){
//
//});

class MobileMenu{
    constructor(){
        //select the element
        this.menuIcon = document.querySelector(".mobile-header-icon");
        this.mobileHeader = document.querySelector(".mobile-menu");
        this.events();
    }
    
    events(){
        this.menuIcon.addEventListener("click", ()=>this.toggleMenu());
    }
    toggleMenu(){
//        console.log("clicked!");
        this.mobileHeader.classList.toggle("mobile-menu-active");
        this.menuIcon.classList.toggle("mobile-header-icon-close");
    }
}

export default MobileMenu;